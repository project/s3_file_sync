(function ($) {
  Drupal.behaviors.s3FileStorage = {
    attach: function(context, settings) {
      $('select#edit-url-method').change(function(e) {
        if ($(this).val() == 'cname') {
          $('.form-item-url-cname input#edit-url-cname').removeAttr("disabled");
          $('.form-item-url-cname').removeClass('form-disabled');
        } else {
          $('.form-item-url-cname input#edit-url-cname').attr("disabled", "disabled");
          $('.form-item-url-cname').addClass('form-disabled');
        }
      });
    }
  };
})(jQuery);
