<?php

/**
 * Form callback to configure global settings for this module.
 */
function s3_file_sync_form_configuration() {
  // Add CSS.
  drupal_add_css(drupal_get_path('module', 's3_file_sync') . '/css/s3_file_sync.admin.css');

  // Define form array.
  $form = array();

  $form['readme'] = array(
    '#type' => 'fieldset',
    '#title' => 'General Information',
  );

  $form['readme']['description'] = array(
    '#markup' => t('To enable file uploads for content types to S3, navigate to the field itself
      to set the configuration. Your S3 settings need to be valid, otherwise this module with trigger errors.'),
  );

  // Add S3 settings.
  _s3_file_sync_form_configuration_storage($form);

  // Add file handling settings.
  _s3_file_sync_form_configuration_file_handling($form);

  // Add async processing settings.
  _s3_file_sync_form_configuration_async($form);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  );

  return $form;
}

/**
 * Settings form helper function: S3 Settings.
 */
function _s3_file_sync_form_configuration_storage(&$form) {
  // Determine if all information is available in order to test the connection.
  $s3_connection_props = array(
    'access_key' => variable_get('s3_file_sync_s3_bucket_access_key_id', ''),
    'secret_access_key' => variable_get('s3_file_sync_s3_bucket_secret_access_key', ''),
  );

  // Determine S3 connection properties.
  $props_available = true;
  array_walk($s3_connection_props, function($value) use (&$props_available) {
    if (empty($value)) {
      $props_available = false;
    }
  });

  $form['storage_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'S3 Settings',
  );

  $form['storage_settings']['test_connection_description'] = array(
    '#type' => 'item',
    '#markup' => t('The credentials required to connect to AWS should be stored in your settings.php
    file. Use the key <em>s3_file_sync_s3_bucket_access_key_id</em> for the access key and the key
    <em>s3_file_sync_s3_bucket_secret_access_key</em> for the secret access key. Currently, you have
    to use the same access key and secret access key for all bucket configurations.'),
  );

  // Access Key.
  if (empty($s3_connection_props['access_key'])) {
    $access_key_text = t('<em class="error">Error: Your access key was not found</em>. Please add it to your settings file.');
  } else {
    $access_key_text = t('We found an access key in your settings file ending in <em>!key_end</em>.', array(
      '!key_end' => substr($s3_connection_props['access_key'], -4, 4),
    ));
  }
  $form['storage_settings']['access_key'] = array(
    '#type' => 'item',
    '#title' => 'Access Key',
    '#markup' => $access_key_text,
  );

  // Secret Access Key.
  if (empty($s3_connection_props['secret_access_key'])) {
    $secret_access_key_text = t('<em class="error">Error: Your secret access key was not found</em>. Please add it to your settings file.');
  } else {
    $secret_access_key_text = t('We found a secret access key in your settings file ending in <em>!key_end</em>.', array(
      '!key_end' => substr($s3_connection_props['secret_access_key'], -4, 4),
    ));
  }
  $form['storage_settings']['secret_access_key'] = array(
    '#type' => 'item',
    '#title' => 'Secret Access Key',
    '#markup' => $secret_access_key_text,
  );

  // Existing buckets.
  $buckets = variable_get('s3_file_sync_storage_buckets', array());
  ksort($buckets);

  if (!empty($buckets)) {
    // Display existing bucket data as table.
    $table_header = array(
      array('data' => t('Bucket Name')),
      array('data' => t('Region')),
      array('data' => t('ACL')),
      array('data' => t('URL Method')),
      array('data' => t('Protocol')),
      array('data' => t('Edit')),
    );

    // Populate hood data.
    $table_rows = array();
    foreach ($buckets as $bucket_key => $bucket) {
      $edit_link = l('Edit Bucket Configuration', 'admin/config/media/s3_file_sync/' . $bucket_key . '/edit');
      $table_rows[] = array($bucket['bucket_name'], $bucket['region'], $bucket['acl'], $bucket['url_method'], $bucket['url_protocol'], $edit_link);
    }

    $form['storage_settings']['buckets'] = array(
      '#type' => 'item',
      '#title' => 'Bucket Configurations',
      '#description' => t('When setting up a field to sync its data with S3, you need to select a bucket configuration.'),
      '#markup' => theme('table', array(
        'header' => $table_header,
        'rows' => $table_rows,
      )),
    );
  }

  $form['storage_settings']['bucket_add_new'] = array(
    '#type' => 'item',
    '#title' => 'Add a new bucket configuration',
    '#markup' => l('Click here to create a new bucket configuration', 'admin/config/media/s3_file_sync/add'),
  );
}

/**
 *
 */
function _s3_file_sync_form_configuration_file_handling(&$form) {
  $form['file_handling_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => 'File Deletion Behavior',
    '#description' => t('<b>Warning:</b> While it is possible to change these settings at any time, they should not be changed
      after this module starts working. Doing so might have unintended side-effects.')
  );

  $form['file_handling_fieldset']['preserve_local_derivatives'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preserve all local image derivatives when local image gets uploaded'),
    '#description' => t('By default, locally generated image derivatives are deleted after they get uploaded to S3.
      Checking this box will preserve any image derivatives on the local filesystem. This will not prevent Drupal 
      from generating derivatives locally whenever a derivative is displayed locally.'),
    '#default_value' => variable_get('s3_file_sync_preserve_local_derivatives', 0),
  );

  $form['file_handling_fieldset']['preserve_remote_derivatives'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preserve remote image derivatives when local file gets deleted'),
    '#description' => t('By default, remote image derivatives are deleted when the local file is deleted.
      Check this box to preserve any generated derivatives in your S3 bucket when the related local file gets deleted.
      Checking this box will also preserve any local database records for files that are preserved.
      This settings does not apply to the original file that gets uploaded to your S3 bucket. Checking this box will not delete S3 related
      to local files that have been deleted in the past. Unchecking this box will not recover previously deleted S3 images.
      This setting does not apply to the remote original image.'),
    '#default_value' => variable_get('s3_file_sync_preserve_remote_derivatives', 0),
  );

  $form['file_handling_fieldset']['preserve_remote_original'] = array(
    '#type' => 'checkbox',
    '#title' => t('Preserve remote image original when local file gets deleted'),
    '#description' => t('By default, the remote original file is deleted when the local file is deleted.
      Check this box to preserve the remote original file in your S3 bucket when the related local file gets deleted.
      Checking this box will also preserve any local database records for files that are preserved.
      This settings does not apply to the remote image derivatives file that gets uploaded to your S3 bucket. Checking this box will not delete S3 related
      to local files that have been deleted in the past. Unchecking this box will not recover previously deleted S3 images.
      This setting does not apply to any remote derivative image.'),
    '#default_value' => variable_get('s3_file_sync_preserve_remote_original', 0),
  );
}

/**
 * Helper functions for async settings.
 */
function _s3_file_sync_form_configuration_async(&$form) {
  $form['async'] = array();

  $form['async'] = array(
    '#type' => 'fieldset',
    '#title' => 'Async Settings',
    '#description' => t('Configure settings that control how queues are
      triggered asynchronously after a local file is uploaded.')
  );

  // Defines method to for asynchronous queue triggering.
  $form['async']['async_method'] = array(
    '#type' => 'select',
    '#title' => t('Method'),
    '#description' => t('Select your preferred method for creating the async
      connection. To use Curl, you need to have the !curl extension installed,
      which you !curl_detection.', array(
      '!curl' => l('Curl', 'http://php.net/manual/en/book.curl.php', array(
        'attributes' => array('target' => '_blank'),
      )),
      '!curl_detection' => (function_exists('curl_version')) ? 'do' : 'do not',
    )),
    '#options' => array(
      "fsockopen" => t('Socket Connection (fsockopen)'),
      "curl" => t('Curl'),
    ),
    '#default_value' => variable_get('s3_file_sync_async_method', 'fsockopen'),
  );

  // Defines async timeout.
  $form['async']['async_timeout'] = array(
    '#type' => 'select',
    '#title' => t('Timeout'),
    '#description' => t('Select how many milliseconds the asynchronous call
      should wait before it closes the connection. This time will be added to
      the time it takes to upload a file if "queue and process
      asynchronously" is the method you have selected for any file or image fields.
      The asynchronous processing does not seem to work if the connection
      terminates right away. If processing asynchronously does not work,
      increase this value.'),
    '#options' => array(
      50 =>  '50 ms',
      100 => '100 ms',
      200 => '200 ms',
      300 => '300 ms',
      400 => '400 ms',
      500 => '500 ms',
      600 => '600 ms',
      700 => '700 ms',
      800 => '800 ms',
      900 => '900 ms',
      1000 => '1000 ms',
    ),
    '#default_value' => variable_get('s3_file_sync_async_timeout', 100),
  );

  // Defines time the process that gets kicked off asynchronously should
  // wait before it gets items from the queue table.
  $form['async']['async_process_wait'] = array(
    '#type' => 'select',
    '#title' => t('Async Process Wait Time'),
    '#description' => t('Select how long the async process should wait before
      it reads items from the queue table. To make sure the parent process has
      enough time to write records to the database before the asynchronously
      kicked-off process will read from the database, add some waiting time
      at the beginning of the async process. If processing asynchronously
      does not work, increase this value.'),
    '#options' => array(
      100 => '100 ms',
      250 => '250 ms',
      500 => '500 ms',
      750 => '750 ms',
      1000 => '1000 ms',
      1500 => '1500 ms',
      2000 => '20000 ms',
    ),
    '#default_value' => variable_get('s3_file_sync_async_process_wait', 1000),
  );
}

/**
 * Submit callback for file field configuration form.
 */
function s3_file_sync_form_configuration_submit($form, &$form_state) {
  // Store file handling flags.
  variable_set('s3_file_sync_preserve_local_derivatives', $form_state['values']['preserve_local_derivatives']);

  // Store remote derivatives deletion flag.
  variable_set('s3_file_sync_preserve_remote_derivatives', $form_state['values']['preserve_remote_derivatives']);

  // Store remote original deletion flag.
  variable_set('s3_file_sync_preserve_remote_original', $form_state['values']['preserve_remote_original']);

  // Store async method and timeout.
  variable_set('s3_file_sync_async_method', $form_state['values']['async_method']);
  variable_set('s3_file_sync_async_timeout', $form_state['values']['async_timeout']);
  variable_set('s3_file_sync_async_process_wait', $form_state['values']['async_process_wait']);

  // Add success message.
  drupal_set_message('S3 settings were successfully saved.');
}

/**
 * Test the S3 connection.
 */
function s3_file_sync_form_configure_storage_test_connection($form, &$form_state) {
  // Connect to bucket and get the policy.
  $s3Obj = new S3FileSyncS3($form['metadata']['#value']['s3_props']);
  try {
    $result = $s3Obj->getBucketLocation();
    $locationConstraint = $result->get('LocationConstraint');

    // Verify that the set region matches with the region of this S3 bucket.
    if ($locationConstraint != $form['metadata']['#value']['s3_props']['region']) {
      drupal_set_message(t('The connection test was not successful. Check your settings.'), 'error');
    } else {
      drupal_set_message(t('The connection was successfully tested.'));
    }
  } catch (Exception $e) {
    drupal_set_message(t('The connection test was not successful. Check your settings.<br />@message', array(
      '@message' => $e->getMessage(),
    )), 'error');
  }
}

/**
 * Edit an existing bucket configuration.
 */
function s3_file_sync_form_bucket_edit($form, &$form_state, $mode, $bucket_key = '') {
  // Load existing buckets.
  $buckets = variable_get('s3_file_sync_storage_buckets', array());

  // Make sure bucket key is not empty.
  if ($mode == 'edit' && (empty($bucket_key) || !array_key_exists($bucket_key, $buckets))) {
    drupal_set_message('This is not a valid bucket configuration!', 'error');
    return;
  }

  // Shortcut for current bucket configuration.
  if ($mode == 'edit') {
    $settings = $buckets[$bucket_key];
  } else {
    $settings = array();
  }

  // Add JS / CSS.
  drupal_add_js(drupal_get_path('module', 's3_file_sync') . '/js/s3_file_sync.admin.js');

  // Define form array.
  $form = array();

  // Loop data through to submit function.
  $form['buckets'] = array(
    '#type' => 'value',
    '#value' => $buckets,
  );

  // Set bucket key. For "add" mode, this will be overwritten in validate function.
  $form['bucket_key'] = array(
    '#type' => 'value',
    '#value' => $bucket_key,
  );

  $form['mode'] = array(
    '#type' => 'value',
    '#value' => $mode,
  );

  $form['bucket_configuration'] = array(
    '#type' => 'fieldset',
    '#title' => 'S3 Bucket Configuration',
  );

  // Determine if all information is available in order to test the connection.
  $s3_connection_props = array(
    'access_key' => variable_get('s3_file_sync_s3_bucket_access_key_id', ''),
    'secret_access_key' => variable_get('s3_file_sync_s3_bucket_secret_access_key', ''),
  );

  // Determine S3 connection properties.
  $props_available = true;
  array_walk($s3_connection_props, function($value) use (&$props_available) {
    if (empty($value)) {
      $props_available = false;
    }
  });

  $form['bucket_configuration']['notes'] = array(
    '#type' => 'item',
    '#markup' => t('Setup an existing S3 bucket to be used in a field. 
      The credentials required to connect to AWS should be stored in your 
      settings.php file. Use the key s3_file_sync_s3_bucket_access_key_id 
      for the access key and the key s3_file_sync_s3_bucket_secret_access_key 
      for the secret access key. Currently, you need to use the same access key 
      and secret access key for all bucket configurations.'),
  );

  // Test connection settings.
  if ($mode == 'edit') {
    // Store metadata in form.
    $form['metadata'] = array(
      '#type' => 'value',
      '#value' => array(
        's3_props' => $settings,
      ),
    );

    $description_markup_addtl = (!$props_available) ? t('Connection cannot be tested
    because some configuration settings are not populated.') : t('Click below to test the connection with S3.');
    $form['bucket_configuration']['test_connection_description'] = array(
      '#type' => 'item',
      '#title' => t('Test Current S3 Connection Settings'),
      '#markup' => t('The credentials required to connect to AWS should be stored in your settings.php
    file. Use the key <em>s3_file_sync_s3_bucket_access_key_id</em> for the access key and the key
    <em>s3_file_sync_s3_bucket_secret_access_key</em> for the secret access key. ') . $description_markup_addtl,
    );

    $form['bucket_configuration']['test_connection'] = array(
      '#type' => 'submit',
      '#value' => t('Test Connection'),
      '#disabled' => (!$props_available),
      '#submit' => array('s3_file_sync_form_configure_storage_test_connection'),
    );
  }

  // Bucket name.
  if ($mode == 'edit') {
    $bucket_name_description = t('The bucket name cannot be changed for an existing bucket configuration.');
  } else {
    $bucket_name_description = t('Enter the bucket name for this bucket configuration. Must be at least 3 characters, maximum of 63 characters.');
  }
  $form['bucket_configuration']['bucket_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Bucket Name'),
    '#default_value' => ($mode == 'edit') ? $settings['bucket_name'] : null,
    '#maxlength' => 63,
    '#description' => $bucket_name_description,
    '#required' => true,
    '#disabled' => ($mode == 'edit'),
  );

  // S3 Region.
  $form['bucket_configuration']['region'] = array(
    '#type' => 'select',
    '#title' => t('Region'),
    '#options' => array(
      "ap-northeast-1" => "Asia Pacific (Tokyo)",
      "ap-southeast-2" => "Asia Pacific (Sydney)",
      "ap-southeast-1" => "Asia Pacific (Singapore)",
      "cn-north-1" => "China (Beijing)",
      "eu-central-1" => "EU (Frankfurt)",
      "eu-west-1" => "EU (Ireland)",
      "us-east-1" => "US East (N. Virginia)",
      "us-west-1" => "US West (N. California)",
      "us-west-2" => "US West (Oregon)",
      "sa-east-1" => "South America (Sao Paulo)",
    ),
    '#default_value' => ($mode == 'edit') ? $settings['region'] : 'us-east-1',
    '#description' => t('Select the region of your S3 bucket.'),
  );

  // S3 ACL.
  $canned_acls = array('private', 'public-read', 'public-read-write', 'aws-exec-read', 'authenticated-read', 'bucket-owner-read', 'bucket-owner-full-control', 'log-delivery-write');
  $form['bucket_configuration']['acl'] = array(
    '#type' => 'select',
    '#title' => t('ACL (Access Control List)'),
    '#options' => drupal_map_assoc($canned_acls),
    '#default_value' => ($mode == 'edit') ? $settings['acl'] : 'public-read',
    '#description' => t('Select the ACL for file uploads. Currently only supports canned ACLs. See !doclink for more details.', array(
      '!doclink' => l('documentation', 'http://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html', array(
        'attributes' => array('target' => '_blank'),
      )),
    )),
  );

  $form['url_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'URL Settings',
    '#description' => t('These settings do not affect file uploads to S3. Instead, they 
      affect how the URLs for the files that were uploaded to S3 are generated
      for each file field. After a file is successfully uploaded, all URLs get cached.')
  );

  $form['url_settings']['url_method'] = array(
    '#type' => 'select',
    '#title' => t('URL Method'),
    '#description' => t('Define your preferred URL style according to these 
      options: !documentation_link. 
      If you change these settings, clear your cache because the
      old values get cached with each file field. 
      Here are examples for each style: !examples', array(
      '!documentation_link' => l('AWS documentation options', 'http://docs.aws.amazon.com/AmazonS3/latest/dev/VirtualHosting.html', array(
        'attributes' => array('target' => '_blank'),
      )),
      '!examples' => '<ul>
        <li>Path-Style Method: http://s3.amazonaws.com/[bucket-name]/[object-key]</li>
        <li>Virtual Hosted-Style Method: http://[bucket-name].s3.amazonaws.com/[object-key]</li>
        <li>CNAME Method: [custom_cname]/[object-key]</li></ul>',
    )),
    '#options' => array(
      'path' =>  'Path Style Method',
      'virtual_hosted' => 'Virtual Hosted-Style Method',
      'cname' => 'CNAME Method',
    ),
    '#default_value' => ($mode == 'edit') ? $settings['url_method'] : 'path',
  );

  // HTTPS Settings.
  $form['url_settings']['url_protocol'] = array(
    '#type' => 'select',
    '#title' => t('Protocol'),
    '#description' => t('Select the protocol for generating S3 URLs. Only applies to 
      path-style method and virtual hosted-style method.'),
    '#options' => array(
      'http' =>  'http',
      'https' => 'https',
    ),
     '#default_value' => ($mode == 'edit') ? $settings['url_protocol'] : 'http',
  );

  // Custon CNAME.
  $form['url_settings']['url_cname'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom CNAME'),
    '#default_value' => ($mode == 'edit') ? $settings['url_cname'] : 'https://s3.amazonaws.com',
    '#maxlength' => 255,
    '#disabled' => ($settings['url_method'] != 'cname'),
    '#description' => t('Enter the full custom CNAME you want to use, including the protocol, 
      excluding a trailing slash. Only applicable when URL Method is set to "CNAME Method". 
      Example: <em>http://www.mydomain.com</em>'),
  );

  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#submit' => array('s3_file_sync_form_back'),
    '#limit_validation_errors' => array(),
  );

  if ($mode == 'edit') {
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Bucket Configuration'),
      '#validate' => array('dna_newsletters_form_bucket_delete_validate'),
      '#submit' => array('dna_newsletters_form_bucket_delete_submit'),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => ($mode == 'edit') ? t('Update Bucket Configuration') : t('Save Bucket Configuration'),
  );

  return $form;
}

/**
 * Validation callback for bucket edit form.
 */
function s3_file_sync_form_bucket_edit_validate($form, &$form_state) {
  // Only react when the mode is "add".
  if ($form_state['values']['mode'] == 'add') {
    // Set bucket key to bucket name.
    $form_state['values']['bucket_key'] = $form_state['values']['bucket_name'];

    // Bucket names need to be unique.
    foreach ($form_state['values']['buckets'] as $bucket) {
      if ($bucket['bucket_name'] == $form_state['values']['bucket_name']) {
        form_set_error('bucket_name', 'This bucket name already exists. Please choose a different one.');
      }
    }

    // Connect to bucket and get the policy.
    if (!empty($form_state['values']['bucket_name'])) {
      $s3Obj = new S3FileSyncS3(array(
        'bucket_name' => $form_state['values']['bucket_name'],
        'region' => $form_state['values']['region'],
        'acl' => $form_state['values']['acl'],
        'url_method' => $form_state['values']['url_method'],
        'url_protocol' => $form_state['values']['url_protocol'],
      ));
      try {
        $result = $s3Obj->getBucketLocation();
        $locationConstraint = $result->get('LocationConstraint');

        // Verify that the set region matches with the region of this S3 bucket.
        if ($locationConstraint != $form_state['values']['region']) {
          form_set_error('bucket_configuration', 'The connection test was not successful. Check your settings.');
        }
      } catch (Exception $e) {
        form_set_error('bucket_configuration', t('The connection test was not successful. Check your settings.<br />@message', array(
          '@message' => $e->getMessage(),
        )));
      }
    }
  }
}

/**
 * Validate callback for deleting a bucket.
 */
function dna_newsletters_form_bucket_delete_validate($form, &$form_state) {
  // If bucket is in use in any field, disallow deletion.
  foreach (field_info_fields() as $field) {
    if (
      array_key_exists('s3_file_sync', $field['settings']) &&
      array_key_exists('bucket', $field['settings']['s3_file_sync']) &&
      !empty($field['settings']['s3_file_sync']['bucket_key']) &&
      $field['settings']['s3_file_sync']['bucket_key'] == $form_state['values']['bucket_key']
    ) {
      form_set_error('bucket_name', t('This bucket cannot be deleted because it is in use
      by at least one field: !field_name.', array(
        '!field_name' => $field['field_name'],
      )));
    }
  }
}

/**
 * Submit callback for deleting a bucket.
 */
function dna_newsletters_form_bucket_delete_submit($form, &$form_state) {
  // Delete bucket.
  $buckets = $form_state['values']['buckets'];
  unset($buckets[$form_state['values']['bucket_key']]);
  variable_set('s3_file_sync_storage_buckets', $buckets);

  // Redirect to list page.
  drupal_set_message('Bucket was successfully deleted.');
  $form_state['redirect'] = 'admin/config/media/s3_file_sync';
}

/**
 * Submit callback for bucket add/edit form.
 */
function s3_file_sync_form_bucket_edit_submit($form, &$form_state) {
  // Remove internal form elements.
  form_state_values_clean($form_state);

  // Get existing buckets and unset from values.
  $buckets = $form_state['values']['buckets'];
  $bucket_key = $form_state['values']['bucket_key'];
  $mode = $form_state['values']['mode'];

  // Unset values that shouldn't be stored for this bucket.
  unset($form_state['values']['buckets']);
  unset($form_state['values']['bucket_key']);
  unset($form_state['values']['mode']);
  unset($form_state['values']['metadata']);

  // Update the buckets variable with new data.
  $buckets[$bucket_key] = $form_state['values'];
  variable_set('s3_file_sync_storage_buckets', $buckets);

  // Set success message.
  switch ($mode) {
    case 'add':
      $message = t('Your new bucket was successfully created. !test_link.', array(
        '!test_link' => l('Test your connection settings', 'admin/config/media/s3_file_sync/' . $form_state['values']['bucket_name'] . '/edit'),
      ));
      break;
    case 'edit':
      $message = t('Settings for this bucket were successfully updated.');
      break;
  }
  drupal_set_message($message);

  // Redirect to general settings form.
  $form_state['redirect'] = 'admin/config/media/s3_file_sync';
}

/**
 * Functionality for back button.
 */
function s3_file_sync_form_back($form, &$form_state) {
  drupal_set_message('No changes were made.');
  $form_state['redirect'] = 'admin/config/media/s3_file_sync';
}
