<?php

/**
 * @file
 * API documentation for S3 File Sync.
 */

/**
 * React to a file being dispatched for syncing. This means one of the
 * following events:
 * - file gets processed instantly
 * - file gets added to the queue
 * - file gets processed triggered from a queue process
 *
 * @param string $signal Signal that dispatch receives. Can be one of these:
 *    - queue
 *    - process
 * @param array $params Multi-dimensional array including following keys:
 *    - file (File Object)
 *    - metadata (Metadata for processing)
 *    - queue_triggered (Flag indicating if processing was triggered from queue)
 */
function hook_s3_file_sync_dispatch($signal, $params) {
  // Determine file id.
  $fid = $params['file']->fid;
  
  // Get stored data for file before it's available as a cached
  // version in a field attached to an entity.
  $object_data = s3_file_sync_get_stored_data($fid);
}

/**
 * React to the successfull syncing of a file.
 *
 * This hook gets called when the original file and all
 * image derivatives have been successfully synced to S3.
 * 
 * @param object $file File object that was acted on.
 * @param array $metadata Metadata for file, including table name,
 *    field name, and sync settings.
 */
function hook_s3_file_sync_process($file, $metadata) {
  // Determine file id.
  $fid = $file->fid;

  // Determine entity ID through field name.
  $query = db_select($this->metadata['table_name'], 'ft');
  $query->fields('ft', array('entity_type', 'entity_id'));
  $query->condition($this->metadata['field_column_name'], $this->file->fid);
  $record = $query->execute()->fetchAssoc();
}

/**
 * React to the storage of a new record in the local database.
 *
 * This hook gets called for every new record that gets written or updated
 * to the local database. A record gets written for every file
 * or image derivative that was successfully synced to S3.
 *
 * @param object $record Database recording containing the record id.
 */
function hook_s3_file_sync_write_record($record) {
  // Determine the name of the field table that this record uses.
  // Can be used to load the id of the entity that this field uses.
  $field_name = $record->field_name;
  $field = field_info_field($field_name);
  $field_current_tbl_name = array_keys($field['storage']['details']['sql']['FIELD_LOAD_CURRENT']);
}
