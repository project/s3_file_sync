<?php

/**
 * @file
 *
 * Class to delete files from S3 permanent storage.
 */
class S3FileSyncS3Delete extends S3FileSyncS3 {
  /**
   * @var object $file File object.
   */
  private $file;

  /**
   * @var array $records S3 records for this file.
   */
  private $records = array();

  /**
   * @var bool $preserveRemoteOriginal Flag indicating if remote original
   *   should be preserved when local file gets deleted.
   */
  private $preserveRemoteOriginal = FALSE;

  /**
   * @var bool $preserveRemoteDerivatives Flag indicating if remote derivatives
   *   should be preserved when local file gets deleted.
   */
  private $preserveRemoteDerivatives = FALSE;

  /**
   * @var string $baseDirectory Previously used base directory for file uploads.
   */
  private $baseDirectory = '';

  /**
   * @var array $s3Props S3 connection properties.
   */
  protected $s3Props = array();

  /**
   * Constructor.
   * @param obj $file URI for file upload.
   * @throws Exception
   */
  public function __construct($file) {
    if (!is_object($file) || !property_exists($file, 'fid')) {
      throw new Exception('File is not valid.');
    }

    // Store variable.
    $this->file = $file;

    // Get parameters.
    $this->preserveRemoteOriginal = (boolean) variable_get('s3_file_sync_preserve_remote_original', 0);
    $this->preserveRemoteDerivatives = (boolean) variable_get('s3_file_sync_preserve_remote_derivatives', 0);

    // Get records for this file.
    $this->getRecords();

    // Determine base directory.
    $this->determineBaseDirectory();
  }

  /**
   * Load all S3 records for this file.
   */
  private function getRecords() {
    // Load all entries for this file record.
    $query = db_select('s3_file_sync', 's')
      ->fields('s')
      ->condition('s.fid', $this->file->fid);

    // Get results.
    $results = $query->execute();

    // Convert records into array.
    foreach ($results as $record) {
      $this->records[] = $record;
    }
  }

  /**
   * Determine the base directory.
   * Assume that some records could have been imported from an
   * external system.
   */
  private function determineBaseDirectory() {
    // Only proceed if records exist.
    if (empty($this->records)) {
      return FALSE;
    }

    // Pick a record we want to use to figure out the base path.
    if (count($this->records) > 1 && $this->records[0]->version == 'original') {
      // Pick other than original.
      $record = $this->records[1];
    }
    else {
      // Pick the only record available.
      $record = $this->records[0];
    }

    // Switch between records created by S3 File Sync and others.
    $objectKey = $record->object_key;
    $pathinfo = pathinfo($objectKey);
    if ($pathinfo['filename'] != $record->version) {
      // Record was not generated by S3 File Sync module.
      $this->baseDirectory = $objectKey;
    }
    else {
      // Record was generated by S3 File Sync module.
      $this->baseDirectory = substr($objectKey, 0, strrpos($objectKey, '/'));
    }
  }

  /**
   * Determine bucket name based on local records.
   * Assuming that all derivatives of a single file have been uploaded
   * to the same bucket. Pick the bucket name from the first record.
   */
  private function determineS3Details() {
    $firstRecord = $this->records[0];
    $buckets = variable_get('s3_file_sync_storage_buckets', array());

    // Ensure bucket configuration is available.
    if (!array_key_exists($firstRecord->bucket_key, $buckets)) {
      throw new S3FileSyncProcessorException('Not a valid bucket key');
    }

    // Set s3Props and bucket key.
    $this->s3Props = $buckets[$firstRecord->bucket_key];
  }

  /**
   * Get the base directory for this remote file.
   */
  public function getBaseDirectory() {
    return $this->baseDirectory;
  }

  /**
   * Delete local records and remote files for this file.
   */
  public function cleanupFile() {
    // Only proceed if records exist.
    if (empty($this->records)) {
      return FALSE;
    }

    // Delete local records.
    $this->removeLocalRecords();

    // Delete remote files.
    $this->deleteRemoteFiles();
  }

  /**
   * Remove local records.
   */
  private function removeLocalRecords() {
    // Get IDs that need to be cleaned up.
    $ids = array_map(function ($record) {
      return $record->id;
    }, $this->records);

    $num_deleted = db_delete('s3_file_sync')
      ->condition('id', $ids, 'IN')
      ->execute();

    watchdog('s3_file_sync', t('Delete !count local records for file id !fid.', array(
      '!count' => $num_deleted,
      '!fid' => $this->file->fid,
    )));
  }

  /**
   * Delete remote files for this file.
   *
   * @return object $result AWS result set.
   */
  private function deleteRemoteFiles() {
    // Determine bucket name.
    $this->determineS3Details();

    // Initialize parent using s3Props for bucket name.
    parent::__construct($this->s3Props);

    // Convert objects into correct format for SDK.
    $objects = array();

    // If both remote original and remote derivative should be preserved,
    // skip this record.
    if ($this->preserveRemoteDerivatives && $this->preserveRemoteOriginal) {
      return false;
    }

    // Determine which records should be added to the array of objects
    // that need to be deleted from the S3 remote.
    foreach ($this->records as $record) {
      switch ($record->version) {
        case 'original':
          // If the remote original should be preserved and this is the
          // original record, skip it.
          if ($this->preserveRemoteOriginal) {
            continue;
          }
          break;
        default:
          // If the remote derivative should be preserved and this is
          // a derivative record, skip it.
          if ($this->preserveRemoteDerivatives) {
            continue;
          }
          break;
      }

      // Add record to array of objects to be deleted.
      $objects[] = array(
        'Key' => $record->object_key,
      );
    }

    // If no objects are set do nothing
    if (empty($objects)) {
      return FALSE;
    }

    // Delete file from S3.
    // Even if one, multiple, or all files have already been deleted from S3,
    // the request will be successful.
    $result = $this->s3Client->deleteObjects([
      'Bucket' => $this->s3Props['bucket_name'],
      'Delete' => array(
        'Objects' => $objects,
      ),
    ]);

    watchdog('s3_file_sync', t('Delete !count remote files for file id !fid.', array(
      '!count' => count($result['Deleted']),
      '!fid' => $this->file->fid,
    )));

    // Determine if there were any errors deleting the remote objects.
    $errors = $result->get('Errors');

    // Show a visible message.
    if (!empty($errors)) {
      foreach ($errors as $error) {
        watchdog('s3_file_sync', 'Could not delete the object @object_key due
        to the following error code: @error_code.', array(
          '@object_key' => $error['Key'],
          '@error_code' => $error['Code'],
        ), WATCHDOG_ERROR);
      }
      drupal_set_message('There was an error deleting one or multiple remote
        objects related to this S3 object. Please refer to the log for
        details.', 'error', FALSE);
    }

    return $result;
  }
}
