<?php

/**
 * @file
 *
 * Class that manages integration with S3.
 */

use Aws\Credentials\Credentials;
use Aws\S3\S3Client;

/**
 * File processing class.
 */
class S3FileSyncS3 {
  /**
   * @var array $s3Properties S3 connection properties.
   */
  protected $s3Props = array();

  /**
   * @var object $s3Client S3 Client.
   */
  protected $s3Client;

  /**
   * Constructor.
   *
   * @param array $s3Props Array of S3 properties.
   */
  public function __construct($s3Props = array()) {
    // Validate S3 properties.
    $this->s3Props = $s3Props;
    $this->validateS3Properties();

    // Initialize client.
    $this->initializeS3Client();
  }

  /**
   * Determine properties required for S3 uploads.
   */
  private function validateS3Properties() {
    // Define required properties.
    $required_props = array('bucket_name', 'region', 'acl', 'url_method', 'url_protocol');

    // Required properties cannot be empty.
    foreach ($required_props as $key) {
      if (!array_key_exists($key, $this->s3Props) || empty($this->s3Props[$key])) {
        throw new Exception(sprintf('Value for %s is not valid.', $key));
      }
    }
  }

  /**
   * Get S3 props.
   *
   * @param string $key S3 Props Array key.
   * @return mixed Array or individual key.
   */
  public function getS3Props($key = '') {
    if (empty($key)) {
      return $this->s3Props;
    } else {
      return $this->s3Props[$key];
    }
  }

  /**
   * Get the location of a bucket. Really used as a test function.
   */
  public function getBucketLocation() {
    return $this->s3Client->getBucketLocation(array(
      'Bucket' => $this->s3Props['bucket_name'],
    ));
  }

  /**
   * Initialize S3 client.
   *
   * Use access key id and secret access key from Drupal variables,
   * they should be stored in the settings file for this environment.
   *
   * @see http://docs.aws.amazon.com/aws-sdk-php/v3/guide/guide/credentials.html
   *   for alternative options on how to provide credentials.
   */
  private function initializeS3Client() {
    if (empty($this->s3Client)) {
      // Create default credentials interface.
      $credentials = new Credentials(
        variable_get('s3_file_sync_s3_bucket_access_key_id', ''),
        variable_get('s3_file_sync_s3_bucket_secret_access_key', '')
      );

      // Create a new S3 client.
      $this->s3Client = new S3Client(array(
        'version' => 'latest',
        'region' => $this->s3Props['region'],
        'credentials' => $credentials,
      ));
    }
  }
}
