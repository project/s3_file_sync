<?php

/**
 * Define custom exception class for custom cron class.
 */
class S3FileSyncCronException extends Exception{}

/**
 * File processing class.
 */
class S3FileSyncCron{
  /**
   * @var string $url URL for Curl request.
   */
  private $url = '';

  /**
   * @var int $timeout Maximum number of milliseconds to allow a request
   *   to execute, in milliseconds.
   */
  private $timeout = 100;

  /**
   * Constructor.
   */
  public function __construct() {
    // Determine URL.
    $this->determineUrl();

    // Determine timeout.
    $this->timeout = variable_get('s3_file_sync_async_timeout', 100);

    // Determine how to execute the async call.
    $method = variable_get('s3_file_sync_async_method', 'fsockopen');

    // Only proceed if curl extension is loaded.
    if ($method == 'curl' && extension_loaded('curl')) {
      $this->executeAsyncCurl();
    } else {
      $this->executeFsockopen();
    }
  }

  /**
   * Execute a faux-asynchronous curl request.
   * @see http://www.davenewson.com/dev/methods-for-asynchronous-processes-in-php
   */
  private function executeAsyncCurl() {
    // Initialize curl object.
    $c = curl_init();

    // Set URL.
    curl_setopt($c, CURLOPT_URL, $this->url);

    // Follow the redirects (needed for mod_rewrite)
    curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);

    // Don't retrieve headers or body.
    curl_setopt($c, CURLOPT_HEADER, false);
    curl_setopt($c, CURLOPT_NOBODY, true);

    // Return from curl_exec rather than echoing
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

    // Always ensure the connection is fresh
    curl_setopt($c, CURLOPT_FRESH_CONNECT, true);

    // Allows timeouts of less than 1 seconds on Unix systems.
    // @see http://php.net/manual/en/function.curl-setopt.php#104597
    curl_setopt($c, CURLOPT_NOSIGNAL, true);

    // Timeout super fast once connected, so it goes into async.
    curl_setopt($c, CURLOPT_TIMEOUT_MS, $this->timeout);

    // Execute the curl request.
    curl_exec($c);
  }

  /**
   * Initialize a stream socket using fsockopen.
   * Socket gets closed as soon as endpoint was successfully hit.
   */
  private function executeFsockopen() {
    $parts = parse_url($this->url);

    // Determine port.
    $port = (isset($parts['port'])) ? $parts['port'] : 80;

    // Adjust for SSL.
    if (strtolower($parts['scheme']) == 'https') {
      $hostname = 'ssl://' . $parts['host'];
      $port = 443;
    }
    else {
      $hostname = $parts['host'];
    }

    $fp = fsockopen($hostname, $port, $errno, $errstr, 30);
    if (!$fp) {
      watchdog('s3_file_sync', 'Could not write header for stream socket.');
      return false;
    }

    // Iterate over our stream socket until it
    // 1. Becomes writeable and writes the payload
    // 2. Becomes writeable again
    $connected = true;
    $sent_payload = false;
    while ($connected == true) {
      $read = array('s3_socket' => $fp);
      $write = $read;
      $except = null;

      // Get all available streams.
      $n = stream_select($read, $write, $except, 10);
      if ($n > 0) {
        foreach ($write as $w) {
          // Find our socket.
          $id = array_search($w, $write);
          if ($id == 's3_socket') {
            // Case 2: Initial payload was written, connection became
            // ready for writing again. Once that happens, close it.
            if ($sent_payload) {
              usleep($this->timeout * 1000);
              fclose($w);
              $connected = false;
              break;
            }
            else {
              // Define headers.
              $headers = "GET " . $parts['path'] . '?' . $parts['query'] . " HTTP/1.0\r\n";
              $headers .= "Host: " . $parts['host'] . "\r\n";
              $headers .= "Accept: */*\r\n";
              $headers .= "Content-type: application/json; charset=utf8\r\n";
              $headers .= "Connection: close\r\n\r\n";

              // Write to the socket.
              fwrite($w, $headers);

              // Next time the connection becomes ready for writing, close it.
              $sent_payload = true;
            }
          }
        }
      }
      else {
        watchdog('s3_file_sync', t('Sockets timed out after 30 seconds.'));
        break;
      }
    }
  }

  /**
   * Initialize a stream socket.
   * @see http://board.phpbuilder.com/showthread.php?10391745-Using-stream_socket_client-asynchronously
   */
  private function executeStreamSocket() {
    // Explode out the parameters.
    $url_array = explode("/", $this->url);

    // Is it http or https?
    $method = strtolower(array_shift($url_array));

    // Pop off an array blank.
    array_shift($url_array);

    // Get the host.
    $host = array_shift($url_array);

    // Get the rest of the url.
    $get_params = "/" . implode("/", $url_array);

    // Additional parameters.
    $addr = gethostbyname($host);

    // Set up socket connection parameters.
    switch ($method) {
      case "http:":
        $socket_type = "tcp://";
        $port = ":80";
        break;
      case "https:":
        $socket_type = "ssl://";
        $port = ":443";
        break;
      default:
        echo "Only HTTP and HTTPS protocols currently supported";
        break;
    }

    $timeout = 30;
    $fp = stream_socket_client($socket_type . $addr . $port, $errno, $errstr, $timeout, STREAM_CLIENT_ASYNC_CONNECT);

    if (!$fp) {
      watchdog('s3_file_sync', 'Socket connection to !address not achieve', array(
        '!address' => $socket_type . $addr . $port,
      ), WATCHDOG_ERROR);
      return;
    }

    // If the connection is made, send the request in the header:
    if ($fp !== false) {
      $headers = "GET $get_params HTTP/1.0\r\n";
      $headers .= "Host: $host\r\n";
      $headers .= "Accept: */*\r\n";
      $headers .= "Content-type: application/json; charset=utf8\r\n";
      $headers .= "Connection: close\r\n\r\n";

      // Write to the handler.
      if ($fp !== false) {
        $fwrite = fwrite($fp, $headers);
      }

      // Check to make sure header was correctly written.
      if ($fwrite === false) {
        watchdog('s3_file_sync', 'Could not write header for stream socket.');
      }

      // This line keeps the system from locking up.
      // Set the timeout to 100ms, function expects
      // third parameter in microseconds.
      stream_set_timeout($fp, 0, 100000);

      // Now, retrieve the data.
      // $data = stream_get_contents($fp);

      // Close the connection.
      fclose($fp);
    }
  }

  /*
   * Determine URL.
   */
  private function determineUrl() {
    $parts = array(
      'base_url' => $GLOBALS['base_url'],
      'cron_path' => '/admin/config/media/s3_file_sync/cron',
      'cron_key_separator' => '?cron_key=',
      'cron_key' => variable_get('cron_key', 'drupal'),
    );
    $this->url = implode('', $parts);
  }
}
