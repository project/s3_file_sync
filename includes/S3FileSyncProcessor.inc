<?php

/**
 * Define custom exception class for file processing exception.
 */
class S3FileSyncProcessorException extends Exception{}

/**
 * File processing class.
 * Handles both regular files and images.
 */
class S3FileSyncProcessor {
  /**
   * @var array $styles
   *    Image styles that need processing.
   */
  private $styles;

  /**
   * @var int $file File object.
   */
  private $file;

  /**
   * @var string $fieldName Name of origin field.
   */
  private $fieldName;

  /**
   * @var string $uploadOriginal Flag indicating if original should be uploaded.
   */
  private $uploadOriginal;

  /**
   * @var bool $regenerate Flag indicating if file should be regenerated.
   */
  private $regenerate = false;

  /**
   * @var bool $queueTriggered Flag to determine if processing was initiated by
   *    a queue execution.
   */
  private $queueTriggered = false;

  /**
   * @var obj $s3Client
   *   S3 Client.
   */
  private $s3Client;

  /**
   * @var array $s3Props S3 Properties.
   */
  private $s3Props = array();

  /**
   * @var string $baseDirectoryToken Token for base directory.
   *   Normally defined in image field.
   */
  private $baseDirectoryToken = '';

  /**
   * @var string $baseDirectory Base directory for image uploads.
   */
  private $baseDirectory = '';


  /**
   * Constructor.
   *
   * @param array $args Array of parameters including:
   *   - (int)      fid             File Id
   *   - (array)    metadata        Array of additional metadata.
   *   - (string)   base_directory  Base Directory.
   *   - (bool)     queue_triggered Boolean indicating if triggered by queue.
   *   - (array)    regenerate      (optional) Metadata containing information
   *                                on regenerating derivatives.
   *
   * @throws Exception if uri is not a a string.
   */
  public function __construct($args = array()) {
    // Verify format for file ID.
    if (!is_numeric($args['fid'])) {
      throw new S3FileSyncProcessorException('File ID needs to be an integer.');
    }

    // Load file.
    $this->file = file_load($args['fid']);
    if (!$this->file) {
      throw new S3FileSyncProcessorException('File could not be loaded.');
    }

    // Verify file exists.
    if (!file_exists($this->file->uri)) {
      throw new S3FileSyncProcessorException(sprintf('Source file %s does not exist.', $args['uri']), 520);
    }

    // Verify metadata is available and not empty.
    if (!is_array($args['metadata']) || empty($args['metadata'])) {
      throw new S3FileSyncProcessorException(sprintf('Metadata is missing.'));
    }

    // Verify S3 props are passed.
    if (!is_array($args['metadata']['s3_props']) ||empty($args['metadata']['s3_props'])) {
      throw new S3FileSyncProcessorException(sprintf('S3 Props not available.'));
    }

    // If no base directory token is set, use a default token.
    if (empty($args['metadata']['base_directory_token'])) {
      $args['metadata']['base_directory_token'] = '[current-date:custom:Y]/[current-date:custom:n]/[current-date:custom:U]-[file:fid]';
    }

    // Verify field name is set.
    if (!is_string($args['metadata']['field_name']) || empty($args['metadata']['field_name'])) {
      throw new S3FileSyncProcessorException(sprintf('Field name cannot be empty.'));
    }

    // Set arguments as class properties.
    $this->styles = $args['metadata']['styles'];
    $this->fieldName = $args['metadata']['field_name'];
    $this->baseDirectoryToken = $args['metadata']['base_directory_token'];
    $this->s3Props = $args['metadata']['s3_props'];
    $this->uploadOriginal = $args['metadata']['upload_original'];
    $this->queueTriggered = (array_key_exists('queue_triggered', $args)) ? $args['queue_triggered'] : $this->queueTriggered;
    $this->regenerate = (array_key_exists('regenerate', $args) && is_bool($args['regenerate'])) ? $args['regenerate'] : $this->regenerate;

    // Determine base directory if not passed as metadata.
    if (empty($args['base_directory'])) {
      $this->determineBaseDirectory();
    } else {
      $this->baseDirectory = $args['base_directory'];
    }
  }

  /**
   * Queue file to the database.
   */
  public function queue() {
    // Process on queue run.
    $queue = DrupalQueue::get(S3_FILE_SYNC_PROCESSING_QUEUE);

    // Attempt file to Drupal Queue.
    $success = $queue->createItem(array(
      'fid' => $this->file->fid,
      'field_name' => $this->fieldName,
      'regenerate' => $this->regenerate,
    ));

    // Add logging.
    if ($success) {
      // Log message.
      watchdog('s3_file_sync', 'Successfully queued file id !fid using the URI !uri.', array(
        '!fid' => $this->file->fid,
        '!uri' => $this->file->uri,
      ));

      // Write a database record for the original.
      if ($this->uploadOriginal) {
        $this->writeRecord($this->determineObjectKey('original'), 'original', S3_FILE_SYNC_STATUS_QUEUED);
      }

      // Write a database record for all derivatives.
      if (!empty($this->styles)) {
        foreach ($this->styles as $style_name) {
          $this->writeRecord($this->determineObjectKey($style_name), $style_name, S3_FILE_SYNC_STATUS_QUEUED);
        }
      }
    } else {
      watchdog('s3_file_sync', 'Queuing failed for URI !uri', array('!uri' => $this->file->uri), WATCHDOG_ERROR);
    }
  }

  /**
   * Process file.
   */
  public function process() {
    // Upload the original.
    if ($this->uploadOriginal) {
      $this->syncFile($this->file->uri, $this->determineObjectKey('original'), 'original');
    }

    // Generate and upload image derivatives.
    if (!empty($this->styles)) {
      foreach ($this->styles as $style_name) {
        $this->generateDerivative($style_name);
      }
    }
  }

  /**
   * Add a database record to the local database.
   *
   * @param string $objectKey Object Key.
   * @param string $version Object version.
   * @param int $syncStatus File synchronization status.
   *
   * @throws S3FileSyncProcessorException when database record cannot be updated
   */
  private function writeRecord($objectKey, $version, $syncStatus = S3_FILE_SYNC_STATUS_QUEUED) {
    // If this processing was triggered by a queue and the item
    // is not being regenerated, update an existing record in the
    // database instead of creating a new one.
    if ($this->queueTriggered && !$this->regenerate) {
      // Determine entity ID through field name.
      $query = db_select('s3_file_sync', 's');
      $query->fields('s');
      $query->condition('fid', $this->file->fid);
      $query->condition('version', $version);
      $record = $query->execute()->fetchObject();

      // Ensure record exists.
      if (!$record) {
        throw new S3FileSyncProcessorException(t('Could not find record for fid %fid and version %version', array(
          '%fid' => $this->file->fid,
          '%version' => $version,
        )));
      }

      // Update existing database record.
      if ($record) {
        $record->status = $syncStatus;
        drupal_write_record('s3_file_sync', $record, 'id');
      }
    } else {
      // Write a reference to this object to the local database.
      $record = new stdClass();
      $record->fid = $this->file->fid;
      $record->object_key = $objectKey;
      $record->bucket_key = $this->s3Props['bucket_key'];
      $record->field_name = $this->fieldName;
      $record->version = $version;
      $record->timestamp = time();
      $record->status = $syncStatus;
      drupal_write_record('s3_file_sync', $record);
    }

    // Expose hook.
    module_invoke_all('s3_file_sync_write_record', $record);
  }

  /**
   * Sync file to permanent storage system (S3).
   *
   * @param string $uri URI of photo file.
   * @param string $objectKey S3 object key.
   * @param string $version Version name of this file.
   */
  private function syncFile($uri, $objectKey, $version) {
    // Initialize S3 client.
    $this->s3Client = new S3FileSyncS3Upload($uri, $objectKey, $this->s3Props);

    // Upload file.
    $resultObj = $this->s3Client->uploadFile();

    // Write a reference to this object to the local database.
    $this->writeRecord($objectKey, $version, S3_FILE_SYNC_STATUS_SYNCED);
  }

  /**
   * Determine the base directory for the current image URI.
   * If no base directory token was set, this class will use
   * a default token set in the constructor.
   */
  private function determineBaseDirectory() {
    // Set base directory based on token.
    $this->baseDirectory = token_replace($this->baseDirectoryToken, array(
      'file' => file_load($this->file->fid),
    ));
  }

  /**
   * Determine object key for image derivatives.
   * Uppercase extensions will be normalized to lowercase.
   *
   * @param string $filename Filename of image asset.
   * @return string $parts S3 object key.
   * @throws S3FileSyncProcessorException
   */
  private function determineObjectKey($filename) {
    // Filename and base directory are required.
    if (empty($filename) || empty($this->baseDirectory)) {
      throw new S3FileSyncProcessorException('Filename or base directory are not set.');
    }

    // Determine extension for path.
    $path_info = pathinfo($this->file->uri);
    $extension = (isset($path_info['extension'])) ? $path_info['extension'] : '';

    // Array of filename parts.
    $parts = array(
      'directory' => $this->baseDirectory,
      'separator_style_name' => '/',
      '$filename' => $filename,
      'separator_extension' => '.',
      'extension' => strtolower($extension),
    );

    return implode($parts);
  }


  /**
   * Generate a derivative for a single style.
   *
   * @param string $style_name
   *   Style Name.
   *
   * @throws Exception if style could not be loaded.
   * @throws Exception if derivative could not be generated.
   */
  private function generateDerivative($style_name) {
    // Load style.
    $style = image_style_load($style_name);
    if (!is_array($style)) {
      throw new S3FileSyncProcessorException(sprintf('Style "%s" could not be loaded.', $style_name));
    }

    if (variable_get('s3_file_sync_preserve_local_derivatives', 0)) {
      // If originals should be retained, use Drupal functions to create derivatives.

      // Generate target path.
      $target_path = image_style_path($style_name, $this->file->uri);

      // Generate derivative.
      $success = image_style_create_derivative($style, $this->file->uri, $target_path);
      if (!$success) {
        throw new S3FileSyncProcessorException(sprintf('Image derivative for style %s could not be generated at path %s.', $style_name, $target_path));
      } else {
        $this->syncFile($target_path, $this->determineObjectKey($style_name), $style_name);
      }
    } else {
      // If originals should not be retained, use own function to apply images styles.

      // Generate derivative.
      $uri = $this->image_style_create_derivative($style, $this->file->uri);
      if ($uri) {
        // Upload to S3.
        $this->syncFile($uri, $this->determineObjectKey($style_name), $style_name);

        // Delete temporary file.
        @drupal_unlink($uri);
      } else {
        throw new S3FileSyncProcessorException(sprintf('Image derivative for style %s could not be generated at path %s.', $style_name, $uri));
      }
    }
  }

  /**
   * Creates a new temporary image derivative based on an image style.
   * Taken from Drupal's Image module (image.module), but modified so that
   * image styles are only applied without storing the file in the filesystem.
   *
   * @param $style
   *   An image style array.
   * @param $source
   *   Path of the source file.
   *
   * @return bool
   *   TRUE if an image derivative was generated, or FALSE if the image derivative
   *   could not be generated.
   *
   */
  private function image_style_create_derivative($style, $source) {
    // If the source file doesn't exist, return FALSE without creating folders.
    if (!$image = image_load($source)) {
      return FALSE;
    }

    foreach ($style['effects'] as $effect) {
      image_effect_apply($image, $effect);
    }

    // Create temporary file.
    $destination = drupal_tempnam('temporary://', 's3_file_sync');

    if (!image_save($image, $destination)) {
      if (file_exists($destination)) {
        watchdog('image', 'Cached image file %destination already exists. There may be an issue with your rewrite configuration.', array('%destination' => $destination), WATCHDOG_ERROR);
      }
      return FALSE;
    }

    // Add correct file extension based on original file.
    $path_info = pathinfo($this->file->uri);
    $extension = (isset($path_info['extension'])) ? ('.' . $path_info['extension']) : '';
    $new_destination = $destination . strtolower($extension);
    rename($destination, $new_destination);

    return $new_destination;
  }
}
