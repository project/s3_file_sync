<?php

/**
 * Define custom exception class for handler class.
 */
class S3FileSyncDispatcherException extends Exception{}

/**
 * File handler class.
 *
 * Controls dispatching of files for different targets:
 * Handles queuing as well as sending files to S3 processor.
 */
class S3FileSyncDispatcher{
  /**
   * @var array $metadata Metadata for file processing.
   */
  private $metadata = array();

  /**
   * @var object $file File object.
   */
  private $file;

  /**
   * @var string $signal Signal that indicates how this file should be handled.
   */
  private $signal = '';

  /**
   * @var string $baseDirectory Base directory for processing a file.
   *    Only required when processing is not instantaenous.
   */
  private $baseDirectory = '';

  /**
   * @var bool queueTriggered Flag to determine if file should be force-processed.
   */
  private $queueTriggered = false;

  /**
   * Constructor.
   *
   * @param array $args Array of parameters including:
   *   - (object)   file              File Object.
   *   - (string)   field_name        Field name.
   *   - (bool)     regenerate        Optional flag indicating that the original
   *                                  file should be regenerated.
   *   - (bool)     queue_triggered   Flag indicating if the processing for this
   *                                  file is triggered from a queue:
   *                                  In that case, don't queue it again.
   *
   * @throws S3FileSyncDispatcherException.
   */
  public function __construct($args) {
    // Validate passed arguments.
    $this->validateArguments($args);

    // Check for file.
    if (!is_object($args['file'])) {
      throw new S3FileSyncDispatcherException(sprintf('File object could not be loaded.'));
    }
    $this->file = $args['file'];

    // Set regenerate flag.
    $this->regenerate = (array_key_exists('regenerate', $args)) ? $args['regenerate'] : false;

    // Determine if this was triggered from a queue or not.
    $this->queueTriggered = (array_key_exists('queue_triggered', $args) && is_bool($args['queue_triggered'])) ? $args['queue_triggered'] : $this->queueTriggered;

    // Determine metadata.
    $field_name = (array_key_exists('field_name', $args)) ? $args['field_name'] : null;
    $this->metadata = self::determineMetaData($this->file, $field_name);

    // If neither the original or any styles should be uploaded, do nothing.
    if (empty($this->metadata['styles']) && !$this->metadata['upload_original']) {
      // Nothing to do for this file.
      return;
    }

    // Generate derivatives, either instantly or in Drupal queue.
    // Add item to the queue if it is marked for queue processing
    // and if it is not triggered by an item coming from the queue.
    if ($this->metadata['queue_processing'] != 0 && !$this->queueTriggered) {
      $this->signal = 'queue';
    } else {
      $this->signal = 'process';
    }

    // Dispatch file.
    switch ($this->signal) {
      case 'queue':
        $this->queueFile();
        break;
      case 'process':
        $this->determineBaseDirectory();
        $this->processFile();
        break;
    }

    // Implement a hook to let other modules react to a dispatch event.
    module_invoke_all('s3_file_sync_dispatch', $this->signal, array(
      'file' => $this->file,
      'metadata' => $this->metadata,
      'queue_triggered' => $this->queueTriggered,
    ));
  }

  /**
   * Determine metadata.
   *
   * @param object $file File object to be checked.
   * @param string $field_name Field name. Required for a regular
   *   file load, i.e. when the file object does not contain a
   *   key for the source.
   * @return array $metadata Metadata.
   * @throws S3FileSyncDispatcherException.
   */
  public static function determineMetaData($file, $field_name = '') {
    // Ensure argument has valid type.
    if (!is_object($file)) {
      watchdog('s3_file_sync', 'Passed argument needs to be an object.', WATCHDOG_ERROR);
      return false;
    }

    // Setup array for metadata.
    $metadata = array(
      'styles' => array(),
      'field_name' => '',
      'base_directory_token' => '',
    );

    // Find correct field name from field list unless it is passed as an argument.
    if (empty($field_name)) {
      // Remove last 2 elements of source, which contains language and index.
      $source_parts = explode('_', $file->source);
      $source_parts = array_slice($source_parts, 0, -2);
      $metadata['field_name'] = implode('_', $source_parts);
    } else {
      $metadata['field_name'] = $field_name;
    }

    // Load field.
    $field = field_info_field($metadata['field_name']);
    if (empty($field)) {
      throw new S3FileSyncDispatcherException(t('Detected field (%field_name) is not a valid field.', array('%field_name' => $metadata['field_name'])));
    }

    // Determine table name and file column name.
    $table_names = array_keys($field['storage']['details']['sql']['FIELD_LOAD_CURRENT']);
    $metadata['table_name'] = array_shift($table_names);
    $metadata['field_column_name'] = $field['storage']['details']['sql']['FIELD_LOAD_CURRENT'][$metadata['table_name']]['fid'];

    // Determine styles for this field. Only return values that are not empty.
    if (!empty($field['settings']['s3_file_sync']['image_styles'])) {
      $metadata['styles'] = array_filter($field['settings']['s3_file_sync']['image_styles'], function($value) {
        return (!empty($value));
      });
    }

    // Load S3 properties. Add bucket key to array of S3 props.
    $buckets = variable_get('s3_file_sync_storage_buckets', array());
    $metadata['s3_props'] = $buckets[$field['settings']['s3_file_sync']['bucket_key']];
    $metadata['s3_props']['bucket_key'] = $field['settings']['s3_file_sync']['bucket_key'];

    // Determine if files for this field should be processed using queues.
    if (isset($field['settings']['s3_file_sync']['queue_processing'])) {
      $metadata['queue_processing'] = $field['settings']['s3_file_sync']['queue_processing'];
    } else {
      $metadata['queue_processing'] = 0;
    }

    // Determine if original file should be uploaded.
    if (isset($field['settings']['s3_file_sync']['upload_original'])) {
      $metadata['upload_original'] = (bool)$field['settings']['s3_file_sync']['upload_original'];
    } else {
      $metadata['upload_original'] = false;
    }

    // Determine directory token for this field.
    if (!empty($field['settings']['s3_file_sync']['base_directory_token'])) {
      $metadata['base_directory_token'] = $field['settings']['s3_file_sync']['base_directory_token'];
    }

    // Return styles.
    return $metadata;
  }

  /**
   * Queue files for later processing.
   */
  private function queueFile() {
    // Generate derivatives and upload.
    try {
      $fileProcessor = new S3FileSyncProcessor(array(
        'fid' => $this->file->fid,
        'metadata' => $this->metadata,
        'regenerate' => $this->regenerate,
      ));
      $fileProcessor->queue();

      // Attempt to run the custom cron instantly depending on settings.
      if ($this->metadata['queue_processing'] == 2) {
        new S3FileSyncCron();
      }
    } catch (Exception $e) {
      watchdog_exception('s3_file_sync', $e);
      drupal_set_message(t('Queue of file !fid failed. Please check your logs for more details.', array(
        '!fid' => $this->file->fid,
      )), 'error');
    }
  }

  /**
   * Process file. This can be either triggered as part of
   * a file upload, or through the queue system.
   */
  private function processFile() {
    // Generate derivatives and upload.
    try {
      $fileProcessor = new S3FileSyncProcessor(array(
        'fid' => $this->file->fid,
        'metadata' => $this->metadata,
        'base_directory' => $this->baseDirectory,
        'queue_triggered' => $this->queueTriggered,
        'regenerate' => $this->regenerate,
      ));
      $fileProcessor->process();

      // If file sync was triggered by the queue, empty
      // the cache for the parent entity.
      if ($this->queueTriggered) {
        $this->emptyEntityCache();
      }

      // Expose hook.
      module_invoke_all('s3_file_sync_process', $this->file, $this->metadata);

      // Log message.
      watchdog('s3_file_sync', 'Successfully synced file id !fid using the URI !uri.', array(
        '!fid' => $this->file->fid,
        '!uri' => $this->file->uri,
      ));

    } catch (Exception $e) {
      watchdog_exception('s3_file_sync', $e);
      drupal_set_message('File uploads to S3 failed. Please check your logs for more details.', 'error');
    }
  }

  /**
   * Determine the base directory if a file is dispatched for processing
   * and the processing is not happen instantaneous.
   */
  private function determineBaseDirectory() {
    // Preset base directory in 2 cases:
    // 1. File needs to be regenerated
    // 2. File processing was triggered from queue. In that case,
    //    the complete object key should not change.
    if ($this->regenerate === true) {
      // File needs to be regenerated. We can determine the base directory
      // from the deletion of an existing file.
      watchdog('s3_file_sync', 'File (fid = !fid) was scheduled for regeneration.', array(
        '!fid' => $this->file->fid,
      ));

      // Delete file from S3.
      $deletedMetadata = s3_file_sync_delete_file($this->file);
      $this->baseDirectory = $deletedMetadata['base_directory'];
    } else if ($this->queueTriggered) {
      // Determine the base directory based off a record.
      // This assumes that the object key uses a naming convetion
      // where the filename, not the object key, does not
      // contain any forward-slashes.
      $query = db_select('s3_file_sync', 's');
      $query->fields('s', array('object_key'));
      $query->condition('s.fid', $this->file->fid);
      $object_key = $query->execute()->fetchField();
      if ($object_key) {
        $this->baseDirectory = substr($object_key, 0, strrpos($object_key, '/'));
      }
    }
  }

  /**
   * Empty the cache for this entity. This is required in order to get the
   * correct additional data loaded after asynchronous file upload.
   *
   * When files get synced asynchronously, the initial entity load
   * after an image upload will cache the field data. The files have
   * not been synced at that point, which will prevent the S3 URLs from
   * being listed for each file field. Clearing the cache for the entity
   * will regenerate the cache once the file sync process has completed.
   */
  private function emptyEntityCache() {
    // Determine entity ID through field name.
    $query = db_select($this->metadata['table_name'], 'ft');
    $query->fields('ft', array('entity_type', 'entity_id'));
    $query->condition($this->metadata['field_column_name'], $this->file->fid);
    $record = $query->execute()->fetchAssoc();

    // Clear the cache for this entity.
    $cid = 'field:' . $record['entity_type'] . ':' . $record['entity_id'];
    cache_clear_all($cid, 'cache_field');
  }

  /**
   * Validate passed arguments. Required keys cannot be empty.
   *
   * @param array $args Arguments.
   * @throws S3FileSyncDispatcherException.
   */
  private function validateArguments($args) {
    if (!is_array($args)) {
      throw new S3FileSyncDispatcherException(sprintf('Arguments need to be an array.'));
    }

    // Find required keys and ensure they are not empty.
    $empty_required_keys = array();
    array_walk($args, function($value, $key) use (&$empty_required_keys) {
      $required_keys = drupal_map_assoc(array('file', 'uri'));
      if (in_array($key, $required_keys)) {
        if (empty($value)) {
          $empty_required_keys[$key] = $value;
        }
      }
    });

    if (!empty($empty_required_keys)) {
      throw new S3FileSyncDispatcherException(sprintf('One or many required arguments are missing.'));
    }
  }
}
